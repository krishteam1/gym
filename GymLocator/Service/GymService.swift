//
//  GymService.swift
//  GymLocator
//
//  Created by Krish on 20/12/18.
//  Copyright © 2018 krish. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import SwiftyJSON

class GymService: GymServiceProtocol {
    private var url:String? = "https://private-anon-3c1b7f2ff2-fitlgdemo.apiary-mock.com/api/v1/gyms"
    let longitudeParamKey="longitude"
    let latitudeParamKey="latitude"

    init(path: String?) {
        if ProcessInfo.processInfo.environment["XCTestConfigurationFilePath"] != nil {
            self.url = nil
            if let path = path {
                self.url = "file://\(Bundle.main.bundlePath)\(path)"
            }
        }
    }
    func params(location: CLLocationCoordinate2D) -> [String: Any] {
        return [
            longitudeParamKey: location.latitude,
            latitudeParamKey: location.longitude
        ]
    }
    func getGyms(fromLocation location: CLLocationCoordinate2D,
                  onSuccess successCallback: (([Gym]?) -> Void)?,
                  onFailure failureCallback: ((String?) -> Void)?) {
        guard let url  = self.url else {
            return
        }
        Alamofire.request(url, method: .post, parameters: params(location: location))
            .validate()
            .responseJSON { response in
                switch response.result {
                case .success :
                    guard let locations = response.data else {
                        failureCallback?("Data error")
                        return
                    }
                    do {
                        let json = try JSON(data: locations)
                        var gyms = [Gym]()
                        for location in  (json.array ?? []) {
                            gyms.append(Gym(json: location))
                        }
                        successCallback?(gyms)
                    } catch let error {
                        failureCallback?(error.localizedDescription)
                    }
                case .failure(let error):
                    failureCallback?(error.localizedDescription)
                }
        }
    }
    
    

}
