//
//  GymServiceProtocol.swift
//  GymLocator
//
//  Created by Krish on 20/12/18.
//  Copyright © 2018 krish. All rights reserved.
//

import Foundation
import CoreLocation

protocol GymServiceProtocol {
    func getGyms(fromLocation location: CLLocationCoordinate2D,
               onSuccess successCallback: (([Gym]?) -> Void)?,
            onFailure failureCallback: ((String?) -> Void)?)
}
