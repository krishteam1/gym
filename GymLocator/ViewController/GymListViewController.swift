//
//  ViewController.swift
//  GymLocator
//
//  Created by Krish on 20/12/18.
//  Copyright © 2018 krish. All rights reserved.
//

import UIKit
import CoreLocation

class GymListViewController: UIViewController {
    @IBOutlet var tableView : UITableView?
    let cellIdentifier = "gymListCell"
    let locationManager = CLLocationManager()
    var gymListViewModel : GymListViewModel?
    var gyms : [Gym]?
    override func viewDidLoad() {
        super.viewDidLoad()
        gymListViewModel = GymListViewModel(gymService: GymService(path: nil))
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.delegate = self
        
    }

    func LoadData() {
        guard CLLocationManager.locationServicesEnabled(),
            var gymListViewModel = gymListViewModel,
            let coordinates = locationManager.location?.coordinate else {
                return
        }
        gymListViewModel.failureCallback = { error in
            print("\(String(describing: error))")
        }
      
        gymListViewModel.successCallback = { [weak self] data in
            self?.gyms = data
            self?.tableView?.reloadData()
        }
        gymListViewModel.fetchForLocation(withLocation: coordinates)
    }
}

extension GymListViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return gyms?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as? GymNameCell {
            if let gyms = gyms {
                cell.name.text = gyms[indexPath.row].name
                cell.address.text = gyms[indexPath.row].address
            }
            return cell
        }
        return UITableViewCell()
    }
}
extension GymListViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
            self.LoadData()
    }
    
}
