//
//  GymLocatorTests.swift
//  GymLocatorTests
//
//  Created by Krish on 20/12/18.
//  Copyright © 2018 krish. All rights reserved.
//

import XCTest
@testable import GymLocator
import CoreLocation

class GymListViewModelSpec: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test() {
        let expectation = XCTestExpectation(description: "Load data error.")
        let loadJSONPath = "/test1,json"
        let gymService = GymService(path: loadJSONPath)
        var vm = GymListViewModel(gymService: gymService)
        let c2D = CLLocationCoordinate2DMake(10.0, 299.0)
        var gotData = false
        var datacount = 0
        vm.successCallback = { data in
            expectation.fulfill()
            gotData = true
            datacount = data?.count ?? 0
        }
        vm.fetchForLocation(withLocation: c2D)

        wait(for: [expectation], timeout: 4)
        XCTAssert(gotData, "callback called")
        XCTAssert(datacount > 0, "Data can't be loaded")
    }


}
